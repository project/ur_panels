<?php
// $Id: ur_pane_content_type.inc,v 1.1.2.1 2010/01/29 19:54:02 merlinofchaos Exp $

/**
 * @file
 * "No context" sample content type. It operates with no context at all. It would
 * be basically the same as a 'custom content' block, but it's not even that
 * sophisticated.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('User Relationships Panels Pane'),
  'description' => t('User Relationships Panels  -- adds relationship links .'),

  'required context' => new ctools_context_required(t('User'), 'user'),
  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('ur_pane_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'ur_pane_content_type_render',
  // The default context.
  'defaults' => array(),

  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_ur_pane_content_type_edit_form.
  'edit form' => 'ur_pane_content_type_edit_form',

  // Icon goes in the directory with the content type.
  'icon' => 'icon_example.png',
  'category' => array(t('User Relationships'), -9),

  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function ur_pane_content_type_render($subtype, $conf, $args, $context) {
  global $user;
  
  $account = isset($context->data) ? drupal_clone($context->data) : NULL;
  
  $rtid = $conf['relationships'];
  
  $block = new stdClass();
  
	$relationship = user_relationships_type_load($rtid);
	dsm($relationship);

  //look for relationships
  $relationships = user_relationships_load(array('between' => array($user->uid, $account->uid), 'rtid' => 1, 'approved' => 1));
   

  if (count($relationships) === 0) {
			$text = "Add as $relationship->name";
			$link = 'relationship/' . $account->uid . '/request/' . $rtid;
			$query = "destination=user/$account->uid";
      $friender = '<div>'; 
			$friender = l(t($text), $link, array('query'=>$query, 'attributes'=>array('class'=>'user_relationships_popup_link')));
			$friender .= '</div>'; 
  //'<a href="/relationship/'.$account->uid.'/request?destination=user/'.$account->uid.'" title="Add Friend" class="user_relationships_popup_link">Add as friend</a></div>';

  }

  elseif (count($relationships) >0) {
			$text = "Remove as $relationship->name";
			$link = 'user/' . $user->uid . '/relationships/' . $rtid . '/remove';
			$query = "destination=user/$account->uid";
      $friender = '<div>'; 
			$friender = l(t($text), $link, array('query'=>$query,  'attributes'=>array('class'=>'user_relationships_popup_link')));
			$friender .= '</div>'; 
  //$friender = '<div><a href="/user/' .$user->uid. '/relationships/'.$rid.'/remove?destination=user/'.$account->uid.'" title="Remove Friend" class="user_relationships_popup_link">Remove as friend</a></div>';

  }

  // The title actually used in rendering
  //$block->title = check_plain("No-context content type");
  $block->content =  $friender;
  if($account->uid == $user->uid){
    $block->content = '';
  }

  return $block;

}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_ur_pane_content_type_edit_form.
 *
 */
function ur_pane_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];
  $types = user_relationships_types_load();
    $types_arr = array();
    foreach($types as $type){
      $types_arr[$type->rtid] = $type->name;
    }

  $form['relationships'] = array(
    '#type' => 'select',
    '#title' => t('Relationship to use'),
    '#description' => t('Choose which Relationship to use'),
    '#options' => $types_arr,
		'#default_value' => $conf['relationships'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );

  return $form;
}

function ur_pane_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['relationships'] = $form_state['values']['relationships'];
}