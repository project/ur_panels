<?php

/**
 * Implementation of hook_views_default_views().
 */
// Declare all the .view files in the views subdir that end in .view
function ur_panels_views_default_views() {
$views = array();
$view = new view;
$view->name = 'ur_friends';
$view->description = '';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'users';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = TRUE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'requestee_id' => array(
    'label' => 'requestee',
    'required' => 0,
    'id' => 'requestee_id',
    'table' => 'user_relationships',
    'field' => 'requestee_id',
    'relationship' => 'none',
  ),
  'requester_id' => array(
    'label' => 'requester',
    'required' => 0,
    'id' => 'requester_id',
    'table' => 'user_relationships',
    'field' => 'requester_id',
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'name' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_user' => 1,
    'overwrite_anonymous' => 0,
    'anonymous_text' => '',
    'exclude' => 0,
    'id' => 'name',
    'table' => 'users',
    'field' => 'name',
    'relationship' => 'requestee_id',
  ),
));
$handler->override_option('sorts', array(
  'created_at' => array(
    'order' => 'DESC',
    'granularity' => 'second',
    'id' => 'created_at',
    'table' => 'user_relationships',
    'field' => 'created_at',
    'relationship' => 'requestee_id',
  ),
));
$handler->override_option('arguments', array(
  'requester_id' => array(
    'default_action' => 'default',
    'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'breadcrumb' => '',
    'default_argument_type' => 'current_user',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'break_phrase' => 0,
    'not' => 0,
    'id' => 'requester_id',
    'table' => 'user_relationships',
    'field' => 'requester_id',
    'validate_user_argument_type' => 'uid',
    'validate_user_roles' => array(
      '2' => 0,
      '3' => 0,
      '5' => 0,
      '4' => 0,
    ),
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_fixed' => '',
    'default_argument_user' => 0,
    'default_argument_php' => '',
    'validate_argument_node_type' => array(
      'question_answer' => 0,
      'link' => 0,
      'official_video' => 0,
      'photo' => 0,
      'status' => 0,
      'story' => 0,
      'gallery' => 0,
      'gallery_image' => 0,
      'tourdate' => 0,
      'video' => 0,
      'page' => 0,
      'profile' => 0,
    ),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'validate_argument_transform' => 0,
    'validate_user_restrict_roles' => 0,
    'validate_argument_node_flag_name' => '*relationship*',
    'validate_argument_node_flag_test' => 'flaggable',
    'validate_argument_node_flag_id_type' => 'id',
    'validate_argument_user_flag_name' => '*relationship*',
    'validate_argument_user_flag_test' => 'flaggable',
    'validate_argument_user_flag_id_type' => 'id',
    'validate_argument_php' => '',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('cache', array(
  'type' => 'none',
));
$handler->override_option('style_plugin', 'list');
$handler->override_option('style_options', array(
  'grouping' => '',
  'type' => 'ul',
));

  $views[$view->name] = $view;
  return $views;
}
